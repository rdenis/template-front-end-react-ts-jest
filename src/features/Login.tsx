/* eslint-disable react-hooks/exhaustive-deps */
import AuthApiService from 'common/helpers/api/AuthApiService';
import React, { useEffect } from 'react';

import { useLocation } from 'react-router-dom';
import { useTokenContext } from 'store';

function LoginCallBack() {
  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }
  const query = useQuery();
  const [token, setToken] = useTokenContext();
  const code = query.get('code') as string;

  useEffect(() => {
    try {
      if (token?.code) {
        const getTokenUri = async () => {
          const { tokenUri: uri } = await AuthApiService.Instance.getTokenUri(token?.code as string);
          const data = await AuthApiService.Instance.getAuthToken(uri);
          setToken(data);
        };
        getTokenUri();
      }
    } catch (error) {
      console.log(error);
    }
    return undefined;
  }, [setToken, token?.code]);

  useEffect(() => {
    setToken((prev) => {
      if (prev?.code !== code) {
        return { ...token, code };
      }
      return prev;
    });
  }, [token?.code]);

  return <></>;
}
export default LoginCallBack;
