import * as React from 'react';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { Students } from 'store';
import styled from 'styled-components/macro';
import { Loader } from 'common/components';
import StudentMenuActions from './StudentsMenuActions';

type Props = {
  students: Students;
  dispatchCreateOpen: React.DispatchWithoutAction;
  dispatchEditOpen: React.DispatchWithoutAction;
};

const StudentsGrid: React.FC<Props> = ({ students, dispatchCreateOpen, dispatchEditOpen }) => {
  const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 90 },
    {
      field: 'first_name',
      headerName: 'First name',
      width: 150,
      editable: true,
    },
    {
      field: 'last_name',
      headerName: 'Last name',
      width: 150,
      editable: true,
    },
    {
      field: 'position',
      headerName: 'Position',
      type: 'string',
      width: 110,
      editable: true,
    },
    {
      field: 'fullName',
      headerName: 'Full name',
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      width: 160,
      valueGetter: (params: GridValueGetterParams) =>
        `${params.getValue(params.id, 'first_name') || ''} ${params.getValue(params.id, 'last_name') || ''}`,
    },
    {
      field: 'actions',
      headerName: 'Actions',
      width: 80,
      renderCell: (params) => (
        <StudentMenuActions
          dispatchCreateOpen={dispatchCreateOpen}
          dispatchEditOpen={dispatchEditOpen}
          params={params}
        />
      ),
    },
  ];

  return (
    <Container>
      {!students && <Loader />}
      {students && (
        <DataGrid
          rows={students}
          columns={columns}
          pageSize={10}
          rowsPerPageOptions={[10]}
          checkboxSelection
          disableSelectionOnClick
        />
      )}
    </Container>
  );
};

export default StudentsGrid;

const Container = styled.div`
  height: 60vh;
  width: 100%;
`;
