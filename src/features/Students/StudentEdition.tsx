import React, { useMemo } from 'react';
import Drawer from '@mui/material/Drawer';
import { Student, useActiveStudentContext } from 'store';
import { useForm } from 'react-hook-form';
import AddIcon from '@mui/icons-material/Add';

type Props = {
  activeStudent?: Student;
  open: boolean;
  dispatchOpen: React.DispatchWithoutAction;
};

const StudentEdition: React.FC<Props> = ({ activeStudent, open, dispatchOpen }: Props) => {
  const [activeStudentFromContext] = useActiveStudentContext();

  const { register, handleSubmit } = useForm<Student>({
    defaultValues: useMemo(() => activeStudentFromContext, [activeStudentFromContext]),
  });

  const onSubmit = async (data: Student) => {
    try {
      // await ResourceAPI.update('users/clients', activeClient.id, {
      //   ...data,
      //   customer_id: activeCustomerFromContext?.id || activeCustomer.id,
      //   matricule: data.mail,
      // });
      // setSubmitted(true);
      // dispatchOpen();
      // setSubmitted(false);
    } catch (e) {
      console.log(`:scream: Fail to update client : ${e}`);
    }
  };
  //   useEffect(() => {
  //     reset(activeClientFromContext);
  //   }, [activeClientFromContext, reset]);

  //   useEffect(() => {
  //     const getClients = async () => {
  //       try {
  //         const res = await ResourceAPI.fetchByKeyWithCondition('users', 'clients');
  //         setClients(res.data.datas);
  //       } catch (e) {
  //         console.log(`:scream: Fetch clients request failed : ${e}`);
  //       }
  //     };
  //     getClients();
  //   }, [setClients, submitted]);

  return (
    <Drawer anchor="right" open={open} onClose={dispatchOpen}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <article className="drawer-edit-client">
          <h3>Edit client</h3>
          <div className="column">
            <section className="row label-input" key="create-edit-client-fn">
              <label
                id="client_first_name-edit"
                className="label-input"
                title="First Name"
                htmlFor="createblock_client_first_name"
              >
                First Name
                <input
                  id="client_first_name_input"
                  className="active-blue background-grey no-border deliverables-input "
                  defaultValue={activeStudentFromContext?.first_name}
                  {...register('first_name', {
                    maxLength: 20,
                  })}
                />
              </label>
            </section>
            <section className="row label-input" key="create-edit-client-ln">
              <label
                id="client_last_name"
                className="label-input"
                title="Last Name"
                htmlFor="createblock_client_last_name"
              >
                Last Name
                <input
                  id="client_last_name_input"
                  className="active-blue background-grey no-border deliverables-input "
                  defaultValue={activeStudentFromContext?.last_name}
                  {...register('last_name', {
                    maxLength: 20,
                  })}
                />
              </label>
            </section>
            <section className="row label-input" key="create-edit-client-phone">
              <label id="client_phone" className="label-input" title="Phone" htmlFor="createblock_client_phone">
                Phone
                <input
                  id="client_phone_input"
                  type="tel"
                  // eslint-disable-next-line max-len
                  pattern="^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$"
                  required
                  className="active-blue background-grey no-border deliverables-input "
                  defaultValue={activeStudentFromContext?.phone}
                  {...register('phone', {
                    required: 'phone is required',
                    maxLength: 20,
                    minLength: 10,
                  })}
                />
              </label>
            </section>
            <section className="row label-input" key="create-edit-client-position">
              <label
                id="client_position"
                className="label-input"
                title="Position"
                htmlFor="createblock_client_position"
              >
                Position
                <input
                  id="client_position_input"
                  className="active-blue background-grey no-border deliverables-input "
                  defaultValue={activeStudentFromContext?.position}
                  {...register('position', {
                    maxLength: 20,
                  })}
                />
              </label>
            </section>
            <section className="row label-input" key="create-edit-client-mail">
              <label id="client_mail" className="label-input" title="Email" htmlFor="createblock_client_mail">
                Email
                <input
                  id="client_mail_input"
                  type="email"
                  required
                  className="active-blue background-grey no-border deliverables-input "
                  defaultValue={activeStudentFromContext?.mail}
                  {...register('mail', {
                    required: 'Email is required',
                    maxLength: 20,
                    pattern: {
                      value:
                        // eslint-disable-next-line max-len
                        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                      message: 'Please enter a valid email',
                    },
                  })}
                />
              </label>
            </section>
            <section className="row label-input" key="create-edit-client-dpt">
              <label
                id="client_department"
                className="label-input"
                title="Department"
                htmlFor="createblock_client_department"
              >
                Department
                <input
                  id="client_department_input"
                  className="active-blue background-grey no-border deliverables-input "
                  defaultValue={activeStudentFromContext?.department}
                  {...register('department', {
                    maxLength: 20,
                  })}
                />
              </label>
            </section>

            <button type="submit" className="submit-button">
              <AddIcon color="inherit" className="edit-icon" />
              Update Student
            </button>
          </div>
        </article>
      </form>
    </Drawer>
  );
};
export default StudentEdition;
