import React from 'react';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import styled from 'styled-components/macro';
import IconComponent from 'common/helpers/icon';
import { ResourceAPI } from 'common/helpers';
import { Typography } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { GridRenderCellParams } from '@mui/x-data-grid';

const ITEM_HEIGHT = 48;
interface Props {
  dispatchCreateOpen: React.DispatchWithoutAction;
  dispatchEditOpen: React.DispatchWithoutAction;
  params: GridRenderCellParams;
}
const StudentMenuActions: React.FC<Props> = ({ dispatchCreateOpen, dispatchEditOpen, params }) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const [t] = useTranslation();

  console.log(params);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const editClient = async () => {
    // await setActiveClient(activeClient);
    dispatchEditOpen();
    handleClose();
  };

  const deleteClient = async () => {
    try {
      await ResourceAPI.delete('students', params.row.id);
      handleClose();
    } catch (e) {
      console.log(`:scream: Axios request failed: ${e}`);
    }
  };

  return (
    <div>
      <IconButton aria-label="more" aria-controls="long-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            maxHeight: ITEM_HEIGHT * 5.5,
            width: '16ch',
            color: '#616161',
            fontSize: 14,
            backgroundColor: 'white',
            borderRadius: 15,
          },
        }}
      >
        <MenuItem onClick={editClient} key="menuitem_edit">
          <IconContainer>
            <IconComponent name="edit" />
          </IconContainer>
          <Typography>{t('Edit')}</Typography>
        </MenuItem>
        <MenuItem onClick={deleteClient} key="menuitem_trash">
          <Row>
            <IconContainer>
              <IconComponent name="trash" />
            </IconContainer>
            <Typography>{t('Archive')}</Typography>
          </Row>
        </MenuItem>
      </Menu>
    </div>
  );
};
export default StudentMenuActions;

const IconContainer = styled.div`
  display: flex;
  padding-right: 14px;
`;
const Row = styled.div`
  display: flex;
  flex-direction: row;
`;
