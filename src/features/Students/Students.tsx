import React, { useReducer } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components/macro';
import { Layout } from 'common/components/index';
import { Box, Button, Stack, Tab } from '@mui/material';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import { useStudentsContext } from 'store';
import { useTranslation } from 'react-i18next';

import StudentsGrid from './StudentsGrid';
import StudentEdition from './StudentEdition';

const DashboardPage: React.FC<RouteComponentProps> = ({ match }) => {
  const [students] = useStudentsContext();
  const [, dispatchCreateOpen] = useReducer((prev) => !prev, false);
  const [openEdit, dispatchEditOpen] = useReducer((prev) => !prev, false);
  const [value, setValue] = React.useState('1');
  const [t] = useTranslation();

  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
  };

  return (
    <Layout name="Dashboard" path="/dashboard">
      <Heading>
        <Box sx={{ width: '100%', typography: 'body1' }}>
          <TabContext value={value}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
              <TabList onChange={handleChange} aria-label="lab API tabs example">
                <Tab label="Students" value="1" />
              </TabList>
            </Box>
            <TabPanel value="1">
              <Stack spacing={2} direction="row" sx={{ marginBottom: '1rem' }}>
                <Button variant="contained" sx={{ color: 'white' }}>
                  {t('Add')}
                </Button>
                <Button variant="contained" sx={{ color: 'white' }}>
                  {t('Archive')}
                </Button>
              </Stack>{' '}
              <StudentsGrid
                students={students || []}
                dispatchEditOpen={dispatchEditOpen}
                dispatchCreateOpen={dispatchCreateOpen}
              />
            </TabPanel>
          </TabContext>
        </Box>
      </Heading>

      {/* <ClientCreation open={openCreate} dispatchOpen={dispatchCreateOpen} activeCustomer={activeCustomer} /> */}
      {students && <StudentEdition open={openEdit} dispatchOpen={dispatchEditOpen} activeStudent={students[0]} />}
    </Layout>
  );
};

export default DashboardPage;

const Heading = styled.div`
  margin-top: 36px;
`;
