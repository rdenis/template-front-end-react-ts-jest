const frTranslation = {
  Add: 'Ajouter',
  Edit: 'Modifier',
  Archive: 'Archiver',
};

export default frTranslation;
