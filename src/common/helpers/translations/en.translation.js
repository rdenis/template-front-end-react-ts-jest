const EnTranslation = {
  Add: 'Add',
  Edit: 'Edit',
  Archive: 'Archive',
};

export default EnTranslation;
