import React from 'react';
import {
  ArrowUpWUIcon,
  BagIcon,
  CalendarIcon,
  CalendarMiniGreyIcon,
  ChevronDownIcon,
  ChevronRightIcon,
  ChevronUpIcon,
  ClockBlueIcon,
  ClockIcon,
  CrossIcon,
  DuplicateIcon,
  EditIcon,
  FlagIcon,
  FolderIcon,
  ListIcon,
  MazeIcon,
  MenuThreeDotsHoverIcon,
  MenuThreeDotsIcon,
  MenuThreeDotsXAxisIcon,
  MenuWUActionsIcon,
  MinusRedIcon,
  OkIcon,
  PhoneIcon,
  PlusBlueIcon,
  PlusIcon,
  RefusedIcon,
  ScrollIndicator,
  StepperIcon,
  TrashIcon,
  UserIcon,
} from '../icons/index';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const iconTypes: any = {
  arrowUpWU: ArrowUpWUIcon,
  bag: BagIcon,
  calendar: CalendarIcon,
  calendarMiniGrey: CalendarMiniGreyIcon,
  chevronDown: ChevronDownIcon,
  chevronRight: ChevronRightIcon,
  chevronUp: ChevronUpIcon,
  clock: ClockIcon,
  clockBlue: ClockBlueIcon,
  cross: CrossIcon,
  duplicate: DuplicateIcon,
  edit: EditIcon,
  flag: FlagIcon,
  folder: FolderIcon,
  list: ListIcon,
  maze: MazeIcon,
  menuThreeDots: MenuThreeDotsIcon,
  menuThreeDotsHover: MenuThreeDotsHoverIcon,
  menuThreeDotsX: MenuThreeDotsXAxisIcon,
  menuWUActions: MenuWUActionsIcon,
  minusRed: MinusRedIcon,
  phone: PhoneIcon,
  plus: PlusIcon,
  plusBlue: PlusBlueIcon,
  scroll: ScrollIndicator,
  stepper: StepperIcon,
  trash: TrashIcon,
  user: UserIcon,
  ok: OkIcon,
  refused: RefusedIcon,
};

interface Props {
  name: string;
  fill?: string;
}

const IconComponent: React.FC<Props> = ({ children, name, fill, ...props }) => {
  const Icon: string = iconTypes[name];
  return <Icon {...props} />;
};

export default IconComponent;
