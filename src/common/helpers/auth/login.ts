import AuthApiService from '../api/AuthApiService';

export default async function authenticateUser() {
  try {
    const AuthService = AuthApiService.Instance;
    const authUri = await AuthService.getAuthAuthorization();
    const { tokenUri } = await AuthService.getAuthCode(authUri);
    await AuthService.getTokenUri(tokenUri);
  } catch (error) {
    console.log(error);
  }
}
