import axios, { AxiosRequestConfig } from 'axios';
import { Token } from 'store';
import BaseApiService from './BaseApiService';

export default class AuthApiService extends BaseApiService {
  private baseUrl: string;

  static instance: AuthApiService;

  private secret: string;

  private redirectUri: string;

  private axiosAuthConfig: AxiosRequestConfig;

  public static get Instance() {
    // eslint-disable-next-line no-return-assign
    return AuthApiService.instance || (AuthApiService.instance = new AuthApiService());
  }

  constructor() {
    super();
    this.secret = Buffer.from(
      `${process.env.REACT_APP_CognitoClientAppId}:${process.env.REACT_APP_CognitoClientAppSecret}`
    ).toString('base64');

    this.baseUrl = `${process.env.REACT_APP_SERVER_URL}/auth`;
    this.verifyParams();
    this.redirectUri = process.env.REACT_APP_REDIRECT_URI_CODE as string;
    this.axiosAuthConfig = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Origin': '*',
      },
    };
  }

  private verifyParams() {
    if (!this.secret) {
      throw new Error('Missing secret');
    }
  }

  async getAuthAuthorization(): Promise<string> {
    const res = await axios.get(`${this.baseUrl}/authorize?redirect_uri=${this.redirectUri}`, this.axiosAuthConfig);
    return res.data.authUri;
  }

  async getAuthToken(tokenUri: string) {
    const res = await axios.post(
      tokenUri,
      {},
      {
        ...this.axiosAuthConfig,
        headers: {
          ...this.axiosAuthConfig.headers,
          'content-type': 'application/x-www-form-urlencoded',
          Authorization: `Basic ${this.secret}`,
        },
      }
    );
    this.setLocalStorageToken(res.data);
    return res.data;
  }

  async getTokenUri(code: string): Promise<{ tokenUri: string }> {
    const res = await axios.post(
      `${this.baseUrl}/token?code=${code}&redirect_uri=${this.redirectUri}`,
      this.axiosAuthConfig
    );
    return res.data;
  }

  async getAuthCode(authUri: string): Promise<{ tokenUri: string }> {
    return axios.get(authUri, this.axiosAuthConfig);
  }

  async getRefreshTokenUri(refresh_token: string): Promise<{ tokenUri: string }> {
    const res = await axios.post(`${this.baseUrl}/refresh`, { refresh_token });
    return res.data;
  }

  // eslint-disable-next-line class-methods-use-this
  public setLocalStorageToken(data: Token): void {
    const tokenAlreadyPresent = localStorage.getItem('cognito-token');
    let dataToStock = data;
    if (tokenAlreadyPresent) {
      dataToStock = { ...JSON.parse(tokenAlreadyPresent), ...data };
    }
    // eslint-disable-next-line no-param-reassign
    data.expires_at = (Date.now() + (data.expires_in as number)) as number;
    localStorage.setItem('cognito-token', JSON.stringify(dataToStock));
  }
}
