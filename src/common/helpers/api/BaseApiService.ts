class BaseApiService {
  axiosConfig: unknown;

  protected constructor() {
    this.getAxiosConfig();
  }

  // eslint-disable-next-line class-methods-use-this
  private getAxiosConfig() {
    return {
      baseURL: process.env.REACT_APP_SERVER_URL,
      headers: {
        Accept: 'application/json',
        'Access-Control-Allow-Origin': '*',
      },
    };
  }
}
export default BaseApiService;
