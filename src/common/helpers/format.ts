/* eslint-disable comma-dangle */
/* eslint-disable indent */
/* eslint-disable implicit-arrow-linebreak */

const monthsToString = ['none', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

export const formatDateToddMMYYY = (date: string): string =>
  `${date.slice(8, 10)}/${date.slice(5, 7)}/${date.slice(0, 4)}`;

export const formatDateToYYYY = (date: string): string => `${date.slice(0, 4)}`;

export const formatDateLitteral = (date: string): string =>
  `${date.slice(8, 10)} ${monthsToString[Number(date.slice(5, 7))]} ${date.slice(0, 4)}`;

// These generics are inferred, do not pass them in.
export const renameKey = <OldKey extends keyof T, NewKey extends string, T extends Record<string, unknown>>(
  oldKey: OldKey,
  newKey: string,
  userObject: T
): Record<NewKey, T[OldKey]> & Omit<T, OldKey> => {
  const { [oldKey]: value, ...common } = userObject;

  return {
    ...common,
    ...({ [newKey]: value } as Record<NewKey, T[OldKey]>),
  };
};

export const capitalize = (string: string) => string.charAt(0).toUpperCase() + string.slice(1);
