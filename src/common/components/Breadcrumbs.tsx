/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import { Link, Breadcrumbs as MUIBreadcrumbs, Typography } from '@mui/material';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components/macro';

const Breadcrumbs = (props: any) => {
  const {
    history,
    location: { pathname },
  } = props;
  const pathnames = pathname.split('/').filter((x: any) => x);
  return (
    <Container>
      <MUIBreadcrumbs aria-label="breadcrumb" separator="›">
        {pathnames.length > 0 ? (
          <Link onClick={() => history.push('/')} color="textSecondary">
            Home
          </Link>
        ) : (
          <Typography color="textSecondary"> Home </Typography>
        )}
        {pathnames.map((name: string, index: number) => {
          const routeTo = `/${pathnames.slice(0, index + 1).join('/')}`;
          const isLast = index === pathnames.length - 1;
          return isLast ? (
            <Typography key={name} color="textPrimary">
              {name}
            </Typography>
          ) : (
            <Link key={name} onClick={() => history.push(routeTo)} color="textPrimary">
              {name}
            </Link>
          );
        })}
      </MUIBreadcrumbs>
    </Container>
  );
};

export default withRouter(Breadcrumbs);

const Container = styled.div`
  display: flex;
  /* margin-left: 10px; */

  @media (max-width: 1000px) {
    justify-content: flex-start;
    align-items: center;
  }
`;
