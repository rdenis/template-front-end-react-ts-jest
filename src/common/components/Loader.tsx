import React from 'react';
import { CircularProgress } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';

const Loader: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CircularProgress size={80} />
    </div>
  );
};

export default Loader;

const useStyles = makeStyles(() => createStyles({
  root: {
    alignItems: 'center',
    display: 'flex',
    marginRight: "2rem",
    // height: '100vh',
    justifyContent: 'center',
  },
}));
