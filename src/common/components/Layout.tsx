import React from 'react';
import styled from 'styled-components/macro';
import { Breadcrumbs } from 'common/components/index';

interface Props {
  name: string;
  path: string;
  actions?: string[];
}

const Layout: React.FC<Props> = ({ name, path, children }) => (
  <Root>
    <Column>
      <Content id="container-content">
        <Breadcrumbs />
        {children}
      </Content>
    </Column>
  </Root>
);

export default Layout;

const Root = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  min-height: 100vh;
  width: 100%;
  flex-grow: 1;
  overflow: hidden;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 768px) {
    margin: 2rem 3%;
  }
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;
