import React from 'react';
import styled from 'styled-components/macro';
// import { Link } from 'react-router-dom';

const Sidebar: React.FC = () => (
      <SidebarNav>
        <Wrapper>
          <div>Sidebar</div>
        </Wrapper>
      </SidebarNav>
);


export default Sidebar;

const SidebarNav = styled.nav`
  background-color: #0063e5;
  display: flex;
  min-width: 180px;
  width: 20vw;
  max-width: 295px;
  flex-shrink: 0;
  min-height: 100vh;
  transition: 350ms;
  justify-content: center;
  align-items: center;

  @media (max-width: 1150px) {
    width: 2rem;
  }
`;

const Wrapper = styled.nav`
  width: 100%;
  height: 100%;
  min-height: 100vh;
`;

