Feature: Catalogs
    Scenario: Display the catalogs
    Given I go to the "catalogs" page
    And I can see the "Catalogs" title
    And I can see a list of "catalog"
    And the "first" "catalog" title should be "V1 2021"
    And the "second" "catalog" title should be "V2 2021"

